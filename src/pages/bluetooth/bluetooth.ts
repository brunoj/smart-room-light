import { Component } from '@angular/core';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';
import { AlertController } from 'ionic-angular';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {TabsPage} from '../tabs/tabs';
import {ViewController} from 'ionic-angular';
@Component({
  selector: 'page-bluetooth',
  templateUrl: 'bluetooth.html'
})
export class BluetoothPage {
  unpairedDevices: any;
  pairedDevices: any;
  Device: any;
  connected = false;
  gettingDevices: Boolean;
  constructor(private bluetoothSerial: BluetoothSerial,
    public navCtrl: NavController,
    private alertCtrl: AlertController,
  private viewCtrl: ViewController,

public navParams: NavParams) {
    bluetoothSerial.enable();
    this.startScanning();
  }

  startScanning() {
    this.pairedDevices = null;
    this.unpairedDevices = null;
    this.gettingDevices = true;
    this.bluetoothSerial.discoverUnpaired().then((success) => {
      this.unpairedDevices = success;
      this.gettingDevices = false;
      success.forEach(element => {
        // alert(element.name);
      });
    },
      (err) => {
        console.log(err);
      })

    this.bluetoothSerial.list().then((success) => {
      this.pairedDevices = success;
        this.viewCtrl._didEnter();
    },
      (err) => {

      })
  }
  success = (data) =>{ alert(data),    this.navCtrl.push(TabsPage)};

  fail = (error) => alert(error);

  selectDevice(address: any) {

    let alert = this.alertCtrl.create({
      title: 'Connect',
      message: 'Do you want to connect with?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Connect',
          handler: () => {
            this.bluetoothSerial.connect(address).subscribe(()=>{
              this.success,
              this.connected=true;

            }, this.fail);
          }
        }
      ]
    });
    alert.present();

  }
  change(){
    this.connected = !this.connected
  }
  disconnect() {
    let alert = this.alertCtrl.create({
      title: 'Disconnect?',
      message: 'Do you want to Disconnect?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Disconnect',
          handler: () => {
            this.bluetoothSerial.disconnect();
            this.connected=false;

          }
        }
      ]
    });
    alert.present();
  }

}

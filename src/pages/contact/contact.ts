import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BluetoothSerial } from '@ionic-native/bluetooth-serial';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {
  success = (data) => alert(data);
  fail = (error) => alert(error);
  level:string;
  events: Events;
  constructor(public navCtrl: NavController,private bluetoothSerial: BluetoothSerial) {
    this.level="0";
    this.bluetoothSerial.write(manual.toString()).then(this.success,this.fail);
    this.events = events;

  }

  send(){
    alert("zmiana wyslana "+this.level);
    this.bluetoothSerial.write(this.level.toString()).then(this.success,this.fail);

  }

}

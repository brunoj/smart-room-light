import { Component } from '@angular/core';
import {Page, ViewController, NavController, Events } from 'ionic-angular';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController,  public events: Events) {

  }
  onPageDidEnter() {
    console.log("Dashboard page load event fired.");
  }
}
